# Opacity-animation #

画面上の特定位置に`target`クラスをつけた要素が来た場合、一番最初の`target`についてはその場に`fixed`し、それ以降の`target`についてはさらに`opacity: 1`にするアニメーションを実行します。

## Usage ##
実際にfixed、アニメーションしたい画像や要素に`target`クラスをつけ、それを`target-wrapper`クラスのついた`div`で囲んでください。

一番最初(HTMLの表示上で一番上にくる、fixされる要素)については、`target`クラスの他に`first-target`クラスをつけてください。