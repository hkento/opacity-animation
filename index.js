$(function() {
  //TODO: data属性でtargetの順番をつける仕様に変更する
  //ターゲットとなる要素が画面上の一定の高さに届いたらその位置にfixedし、opacityアニメーションで表示します
  var fixHeight = $(window).height() * (1 / 2); //この変数で固定する高さを指定します (デフォルトでは画面の1/2の高さに固定します)

  var $allTargets = $(".target");
  var $allTargetWrappers = $(".target-wrapper");
  var firstTargetWrapper = $allTargetWrappers[0];
  var $allTargetWrappersWithoutFirst = $allTargetWrappers.slice(1);

  initFix($allTargets, fixHeight);

  $(window).scroll(function() {
    toggleFixReachedTo(firstTargetWrapper, fixHeight);

    $allTargetWrappersWithoutFirst.each(function() {
      toggleOpacityAnimationReachedTo(this, fixHeight);
    });
  });

  //消えている要素のbottomをfixHeightに設定します
  function initFix($allTargets, fixHeight) {
    $allTargets.each(function() {
      if (this !== $allTargets[0]) { $(this).css("bottom", fixHeight); }
    });
  }

  function toggleOpacityAnimationReachedTo($targetWrapper, fixHeight) {
    var $target = $($targetWrapper).find(".target");
    if (isReachedTo($targetWrapper, fixHeight)) {
      // showOpacity();
      $target.addClass("opacity-animation");
    } else {
      // hideOpacity();
      $target.removeClass("opacity-animation");
    }
  }

  //fixHeight(px)のウインドウの高さに到達したtargetWrapperのtargetをfixし、
  //戻ってfixHeightから離れたらfixをはずします
  function toggleFixReachedTo(targetWrapper, fixHeight) {
    var $target = $(targetWrapper).find(".target");
    if (isReachedTo(targetWrapper, fixHeight)) {
      $target.addClass("fixed");
      $target.css("bottom", fixHeight);
    } else {
      $target.removeClass("fixed");
      $target.css("bottom", "");
    }
  }

  //targetWrapperが画面のfixHeight(px)の高さに届いたか判定します
  function isReachedTo(targetWrapper, fixHeight) {
    var targetWrapperAbslYPos = getAbsoluteYPosition(targetWrapper);
    var scrollPos = window.pageYOffset;
    var windowHeight = $(window).height();
    var targetHeight = castStrPxToNum($(targetWrapper).find(".target").css("height"));

    return scrollPos > (targetWrapperAbslYPos - windowHeight + fixHeight + targetHeight);
  }

  function getAbsoluteYPosition(target) {
    var rect = target.getBoundingClientRect();
    return rect.top + window.pageYOffset;
  }

  //"100px"のような文字列を100のような数値に変換します
  function castStrPxToNum(strPx) {
    return Number(strPx.substr(0, strPx.length -2));
  }

  function debugIsReachedToFirstTarget(targetWrapper, targetWrapperAbslYPos, scrollPos, windowHeight, targetHeight) {
    if ($(targetWrapper).find(".target").attr("id") === "first-target") {
      console.group();
      console.log("targetWrapperAbslYPos: " + targetWrapperAbslYPos);
      console.log("scrollPos: " + scrollPos);
      console.log("windowHeight: " + windowHeight);
      console.log("targetHeight: " + targetHeight);
      console.groupEnd();
    }
  }
});
